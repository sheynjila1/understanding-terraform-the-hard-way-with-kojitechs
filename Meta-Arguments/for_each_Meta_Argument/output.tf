################################################################################
# OUTPUT A SPECIFIC  PUBLIC SUBNET ID
################################################################################
output "public_subnet1_id" {
    value = aws_subnet.public_subnet["public_subnet_1"].id
}

################################################################################
# OUTPUT ALL  PUBLIC SUBNET IDS IN FOR_EACH
################################################################################
output "all_public_subnet_ids" {
    value = [for sub in aws_subnet.public_subnet: sub.id] # WORKS FOR BOTH COUNT && FOR_EACH ONLY
}