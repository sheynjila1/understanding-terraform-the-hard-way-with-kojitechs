
## 

## 
# <BLOCKTYPE>  <RESOURCE NAME> <LOCAL NAME> 
# resource    "aws_instance"   "web" {
 
# <PARAMETER> = <EXPRESSION> # Argument  

# }
# surity_

# resource "aws_instance" "web" {
#   ami           = var.ami_id # " "
#   instance_type = "t3.micro" # ""

#   tags = {
#     Name = "HelloWorld"
#   }
# }

# 3 (7) # Correct 
# 4, (5)

# instance_type: str
# ami: str
# tags = {
#     Name = "HelloWorld"
# }

