
variable "ami_id" {
  type        = string
  description = "ami id"
  default     = "ami-09d3b3274b6c5d4aa"
}

variable "vpc_cidr" {
  type        = string
  description = "vpc cidr"
  default     = "10.0.0.0/16"
}

variable "private_subnets_cidr" {
  description = "private vpc cidr"
  type        = list(any)
  default     = ["10.0.1.0/24", "10.0.3.0/24"] # 5
}

variable "public_subnets_cidr" {
  description = "public vpc cidr"
  type        = list(any)
  default     = ["10.0.0.0/24", "10.0.2.0/24", "10.0.4.0/24"]
}

# variable "create_vpc" {
#   type = bool
#   description = "(optional) describe your variable"
#   default= true 
# }
## how to create a map variable 


## how number variable 
# [a,b,c,d,e,f,g]
#  0,1,2,3,4,5,6
# 