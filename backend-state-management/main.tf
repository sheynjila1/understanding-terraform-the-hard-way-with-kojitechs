

terraform {
required_version = ">=1.1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}
################################################################################
# CREATE AN S3 BUCKET FOR STATEFILE MANAGEMENT
################################################################################

resource "aws_s3_bucket" "this" {
count = length(var.bucket_name)

  bucket = var.bucket_name[count.index]
  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_s3_bucket_policy" "allow_access_from_df_accounts" {
  count  = length(var.bucket_name)
  bucket = aws_s3_bucket.this[count.index].id
  policy = data.aws_iam_policy_document.allow_access_from_another_account[count.index].json
}

data "aws_iam_policy_document" "allow_access_from_another_account" {
   count  = length(var.bucket_name)
  statement {
    effect = "Allow"

    principals {
      type = "AWS"
      identifiers = var.account_role_arns 
    }

    actions = [
      "s3:GetObject",
      "s3:PutObject",
      "s3:ListBucket",
      "s3:DeleteObject",
    ]
    resources = [
      aws_s3_bucket.this[count.index].arn,
      join("/", [aws_s3_bucket.this[count.index].arn, "*"])
    ]
  }
}

resource "aws_iam_policy" "terraform-statelock" {
  name   = "${var.component_name}-terraform-state"
  policy = data.aws_iam_policy_document.allow_access_to_dynamo_from_another_accountA.json
}

resource "aws_iam_role" "my_dynamodb_appautoscaling" {
  name               = "${terraform.workspace}-dynamodb_appautoscaling"
  assume_role_policy = data.aws_iam_policy_document.appautoscaling_assume_role_policy.json
}

data "aws_iam_policy_document" "appautoscaling_assume_role_policy" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

   principals {
      type = "AWS"
      identifiers = var.account_role_arns
    }

  }
}

data "aws_iam_policy_document" "allow_access_to_dynamo_from_another_accountA" {
  statement {
    effect = "Allow"

    actions = [
      "dynamodb:GetItem",
      "dynamodb:PutItem",
      "dynamodb:DeleteItem",
    ]
    resources = [
      aws_dynamodb_table.dynamodb-terraform-lock.arn
    ]
  }
}

################################################################################
# CREATE AN S3 BUCKET FOR STATEFILE MANAGEMENT
################################################################################

resource "aws_dynamodb_table" "dynamodb-terraform-lock" {
  name           = "terraform-lock-prod"
  hash_key       = "LockID"
  read_capacity  = 20
  write_capacity = 20

  attribute {
    name = "LockID"
    type = "S"
  }
   lifecycle {
    prevent_destroy = true
  }
}


