
################################################################################
# CREATE AN S3 BUCKET FOR STATEFILE MANAGEMENT
################################################################################

variable "component_name" {
    type = string
    description = "(optional) describe your variable"
    default = "s3-state-file-configuration"
}

variable "bucket_name" {
    type = list
    description = "list of all buckets"
    default = [
        "understanding-terraform-the-hard-way-with-kojitechs",
        "3-tier-architecture-implementation",
        "enterprise-account-implementation"
        ]
}

variable "account_role_arns" {
    type = list
    description = "(optional) describe your variable"
    default = [
        "arn:aws:iam::181437319056:role/cross-account-terraform-role",
        "arn:aws:iam::735972722491:role/cross-account-terraform-role",
        "arn:aws:iam::674293488770:role/cross-account-terraform-role"
    ]
}